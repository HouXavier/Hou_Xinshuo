### 侯新烁

#### 项目介绍
日后完成的推文放置于此处

**Note：** 写作之前请务必先查看 [「Wiki」](https://gitee.com/Stata002/StataSX2018/wikis/Home) 页面中的相关建议和要求，尤其是 [「Stata连享会推文格式要求」](https://gitee.com/Stata002/StataSX2018/wikis/00_Stata%E8%BF%9E%E4%BA%AB%E4%BC%9A%E6%8E%A8%E6%96%87%E6%A0%BC%E5%BC%8F%E8%A6%81%E6%B1%82.md?sort_id=953475)


## 使用指引
- 请先阅读 [Stata连享会成员手册](https://gitee.com/Stata002/StataSX2018/wikis/Home)，了解 Markdown 和码云的使用方法。
- 然后，到 [【待领取任务】](https://gitee.com/Stata002/StataSX2018/tree/master/_000%E5%BE%85%E9%A2%86%E5%8F%96%E4%BB%BB%E5%8A%A1) 页面中选择自己感兴趣的题目，告知我，我们讨论推文的写作大纲。
- 当然，你也可以自行提出选题，我们做进一步讨论。
