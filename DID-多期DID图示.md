
> 作者：侯新烁 ｜ 连玉君 ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)) 

> Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

> **编者按：** 多期 DID 模型相对于两期 DID 模型在研究中更为常见，因其涉及到不同时期的效应变化，因此，我们也常常需要对其动态的处理效应进行识别——滞后效应和提前效应，本期为大家介绍在 Stata 中如何进行多期经验研究策略的识别，并绘制 DID 效应验图。

> 资料参考来源：[mostly-harmless-replication - 05 Fixed Effects, DD and Panel Data](https://github.com/vikjam/mostly-harmless-replication/blob/master/05%20Fixed%20Effects%2C%20DD%20and%20Panel%20Data/05%20Fixed%20Effects%2C%20DD%20and%20Panel%20Data.md)

---

## 1. 导言

双重差分模型（ Difference in Differences , DID ）是政策效果评估中常用的一种计量识别策略。其原理是基于反事实框架来评估政策发生和不发生这两种情景下被观测变量（因变量）的变化，因而样本被划分为实验组（ Treated 或 Treat ）和对照组（ Untreated 或 Control ）。早期 Card（1992） 使用 PA 和 NJ 快餐店样本展开最低工资影响就业问题的经典研究是基于两期数据的 DID 分析，而当多期数据出现时，该如何对 DID 模型进行图示呢？本次推文，将结合 `coefplot` 命令为大家介绍多期DID模型的图形绘制。

## 2. 资源准备

#### 2.1 基本原理图示

在仅有两期的DID模型中，我们通常使用如下图形展示其原理，以 2008 年为政策发生时间点为例：

![两期 DID ](https://images.gitee.com/uploads/images/2018/1204/000019_363da22a_2109590.jpeg "两期DID原理.jpg")

其中，C 对应 Control 组，T 对应 Treat 组，TD 代表 T的反事实情形，则此时 T 与 TD 在 2008 年的差值即为 DID 识别的处理效应，而差值实际上是 T 组两个时点的差异将去 C 组两个时点的差异，因而被称为双重差分。

而在多期情形中，图形可能展示如下：

![多期 DID 原理图](https://images.gitee.com/uploads/images/2018/1204/000054_ee0bd323_2109590.jpeg "多期DID原理图.jpg")

多个时间节点信息的出现，使得可以对不同样本中组因变量的变化趋势进行观察，也即政策可能在不同时点上对 T 组形成不同影响。借助于 Granger（1969）的思想，观察原因是否发生在结果之前，而不是相反。因此假定感兴趣的政策在不同地区、不同时点上会发生变化，因此在模型中引入不同时点政策变量，因而允许存在滞后效应以及提前效应。同时可能会察觉随着时间的推移，因果效应会减弱或者加强。

接下来，本推文将给出 Stata 多期 DID 的代码操作和图形输出。


#### 2.2  安装 coefplot
 
 该命令由 Ben Jann （ University of Bern, ben.jann@soz.unibe.ch ）于2017年9月份编写，用于对回归系数和其他结果进行图形绘制，展示系数结果和置信区间范围，更多信息请 `help coefplot` 。若您的电脑尚未安装该命令，请执行如下代码：

```stata
ssc install coefplot, replace
```

*若安装出现问题请使用 findit 命令手动安装

```stata
findit coefplot
```

## 3. 多期 DID 图示
 
本期推介的是由 vikjam 编写的 Replication of tables and figures from "Mostly Harmless Econometrics" in Stata, R, Python and Julia 中关于 DID 的图形输出部分代码。该数据源于 Autor（2003）参见：David H. Autor. Outsourcing at Will: The Contribution of Unjust Dismissal Doctrine to the Growth of Employment Outsourcing[J]. Journal of Labor Economics, 2003, 21(1):42.

#### step 1 准备与数据下载

```stata
clear all
set more off
eststo clear
capture version 14

/* 下载 zip 压缩包数据文件并解压 */
shell curl -o outsourcingatwill_table7.zip "http://economics.mit.edu/~dautor/outsourcingatwill_table7.zip"
unzipfile outsourcingatwill_table7.zip
```

此时，若正常运行，则会在 dos 窗口中看到如图情形。

![下载zip数据](https://images.gitee.com/uploads/images/2018/1204/001057_1b656072_2109590.jpeg "下载zip数据.jpg")


解压后得到文件夹 table7 ，其中的  `autor-jole-2003.dta` 数据即为本次示例数据。经验证，部分 Stata 程序可能不能正确执行 `dos` 下载过程，我们需要将网址 http://economics.mit.edu/~dautor/outsourcingatwill_table7.zip 复制到浏览器进行下载，然后手动解压获得数据文件。

![解压后文件夹](https://images.gitee.com/uploads/images/2018/1204/001122_90b76c45_2109590.jpeg "解压后.jpg")

#### step 2 调用数据并对数据进行预处理


```stata
use "table7/autor-jole-2003.dta", clear

*作者对数据进行了论文的仿制处理，具体经济含义请参考原文，或者请忽略，因为不是敲黑板呦~
/* 对数总就业人数：来自劳工统计局（BLS）就业和收入数据 */
gen lnemp = log(annemp)

/* 非商业服务部门就业：来自美国海关（ CBP ） */
gen nonemp  = stateemp - svcemp
gen lnnon   = log(nonemp)
gen svcfrac = svcemp / nonemp

/* 商业服务部门总就业：来自美国海关（ CBP ） */
gen bizemp = svcemp + peremp
gen lnbiz  = log(bizemp)

/* 生成时间趋势 */
gen t  = year - 78 // 线性时间趋势
gen t2 = t^2       // 二次时间趋势

/* 限制样本时间区间 */
keep if inrange(year, 79, 95) & state != 98

/* 生成更多的统计数据 */
gen clp     = clg + gtc
gen a1624   = m1619 + m2024 + f1619 + f2024
gen a2554   = m2554 + f2554
gen a55up   = m5564 + m65up + f5564 + f65up
gen fem     = f1619 + f2024 + f2554 + f5564 + f65up
gen white   = rs_wm + rs_wf
gen black   = rs_bm + rs_bf
gen other   = rs_om + rs_of
gen married = marfem + marmale

/* 修正工会变量 */
replace unmem = . if inlist(year, 79, 81) 
replace unmem = unmem * 100              
```


#### step 3 估计 DID 模型

Autor（2003）在该项考察雇佣保护措施如何影响企业临时工雇佣行为的研究中，使用了多期 DID 的分析方法。在美国，州立法机关公布的雇佣保护措施使得企业解雇工人变得更为困难，作为一种规则，美国劳动法允许随意雇佣，这意味着只要雇主乐意，解雇工人将是不受限制的。但是，在某些州，法院允许不满足随意雇佣原则的“例外”存在，这就导致雇员有权采用不正当解雇来起诉雇主的解聘行为。 那么，企业是否会因为害怕打官司而倾向于雇佣临时工？因为临时工在被解雇时，无法使用不正当解雇的理由来起诉， Autor 正是对这一问题进行了研究。

研究策略在于，将一个州里的临时工就业量作为被解释变量，用虚拟变量来表示该州法庭是否允许随意雇佣条例做出“例外”修改 ，然后在双重差分模型中进行考察。在 DID 模型中，既有该虚拟变量的滞后期，又有其提前期。实际估计中采用了虚拟变量的两期提前期和四期滞后期。数据中，各政策虚拟变量提前期与滞后期变量是已经准备好的，其中，以 `admico` 开头带 `_`  的表示政策提前期虚拟变量，如 `admico_2` 表示政策提前 2 期虚拟变量；以 `admico` 开头不带 `_`  的表示政策滞后期虚拟变量，其中 `admico0` 表示政策发生当期。此时，可以直接将各期变量代入模型展开多期 DID 回归了。**若我们对该方法进行应用时，需要事先对相关政策虚拟变量以及提前与滞后期进行处理**，此处需注意。



![不同时期虚拟变量已经生成](https://images.gitee.com/uploads/images/2018/1204/000514_351617dc_2109590.jpeg "虚拟变量交互项已经生成.jpg")


```stata
reg lnths lnemp admico_2 admico_1 admico0 admico1 admico2 admico3 mico4 admppa_2 admppa_1   ///
    admppa0 admppa1 admppa2 admppa3 mppa4 admgfa_2 admgfa_1 admgfa0 admgfa1 admgfa2 admgfa3 ///
    mgfa4 i.year i.state i.state#c.t, cluster(state)
```

该回归通过 `i.year` 、 `i.state` 和 `i.state#c.t` 对时间固定效应、个体固定效应和个体与时间趋势的交叉效应进行了控制。估计结果表明，在法院允许对随意雇佣条例做出例外修改之前的两年，变量对临时工就业量没有影响，但在允许之后的开始几年里，对临时工就业量产生了剧烈的影响，之后这个影响趋于平坦但维持在较高的水平上。当然，在该例中，很多系数置信区间范围函纳了 0 值，也就是说，在统计意义上并未表现出足够的显著性。

![回归结果展示](https://images.gitee.com/uploads/images/2018/1204/000445_042a6e9d_2109590.jpeg "回归结果.jpg")


#### step 4 图形的输出与优化

使用 `coefplot` 可较为便捷和快速的生成多期动态效应图，更多使用方法请 `help coefplot` 。该图形可以较为直观的展示系数的大小、置信区间以及不同时点上的变化趋势，从而为我们提供提前效应和滞后效应的信息。

```stata
coefplot, keep(admico_2 admico_1 admico0 admico1 admico2 admico3 mico4)   vertical  addplot(line @b @at) 
```

![多期图](https://images.gitee.com/uploads/images/2018/1204/000654_db772162_2109590.jpeg "多期图.jpg")

通过 coefplot 选项对图形进行优化，对横纵坐标的标记、标题以及样式等进行设定：

```stata
coefplot, keep(admico_2 admico_1 admico0 admico1 admico2 admico3 mico4)                     ///
          coeflabels(admico_2 = "2 yr prior"                                                ///
         admico_1 = "1 yr prior"                                                ///
         admico0  = "Yr of adopt"                                               ///
         admico1  = "1 yr after"                                                ///
         admico2  = "2 yr after"                                                ///
         admico3  = "3 yr after"                                                ///
         mico4    = "4+ yr after")                                              ///
         vertical                                                                          ///
         yline(0)                                                                          ///
         ytitle("Log points")                                                              ///
         xtitle("Time passage relative to year of adoption of implied contract exception") ///
         addplot(line @b @at)                                                              ///
         ciopts(recast(rcap))                                                              ///
         rescale(100)                                                                      ///
         scheme(s1mono)
```

![多期图优化](https://images.gitee.com/uploads/images/2018/1204/000635_f886cbce_2109590.jpeg "多期图优化.jpg")


当然，也可以通过 `scheme()` 将风格更改为自己熟悉或常用的格式，例如 `qleanmono` 风格（写为 `scheme(qleanmono)` ，当然要事先安装该风格模版，可参见往期推文  [**Stata绘图: 一个干净整洁的 Stata 图形模板qlean**](https://www.jianshu.com/p/3e6d4f5c0f28) ):


![多期图优化qleanmono](https://images.gitee.com/uploads/images/2018/1204/000717_987a1ad5_2109590.jpeg "多期图优化qleanmono.jpg")

最后，图片的输出保存：

```stata
graph export "figure.png", replace
```

## 4. 代码汇总

```stata
ssc install coefplot, replace
*findit coefplot

clear all
set more off
eststo clear
capture version 14

shell curl -o outsourcingatwill_table7.zip "http://economics.mit.edu/~dautor/outsourcingatwill_table7.zip"
unzipfile outsourcingatwill_table7.zip

use "table7/autor-jole-2003.dta", clear
gen lnemp = log(annemp)

gen nonemp  = stateemp - svcemp
gen lnnon   = log(nonemp)
gen svcfrac = svcemp / nonemp

gen bizemp = svcemp + peremp
gen lnbiz  = log(bizemp)

gen t  = year - 78
gen t2 = t^2      

keep if inrange(year, 79, 95) & state != 98

gen clp     = clg + gtc
gen a1624   = m1619 + m2024 + f1619 + f2024
gen a2554   = m2554 + f2554
gen a55up   = m5564 + m65up + f5564 + f65up
gen fem     = f1619 + f2024 + f2554 + f5564 + f65up
gen white   = rs_wm + rs_wf
gen black   = rs_bm + rs_bf
gen other   = rs_om + rs_of
gen married = marfem + marmale

replace unmem = . if inlist(year, 79, 81) 
replace unmem = unmem * 100              

reg lnths lnemp admico_2 admico_1 admico0 admico1 admico2 admico3 mico4 admppa_2 admppa_1   ///
    admppa0 admppa1 admppa2 admppa3 mppa4 admgfa_2 admgfa_1 admgfa0 admgfa1 admgfa2 admgfa3 ///
    mgfa4 i.year i.state i.state#c.t, cluster(state)

coefplot, keep(admico_2 admico_1 admico0 admico1 admico2 admico3 mico4)   vertical  addplot(line @b @at) 


coefplot, keep(admico_2 admico_1 admico0 admico1 admico2 admico3 mico4)                     ///
          coeflabels(admico_2 = "2 yr prior"                                                ///
         admico_1 = "1 yr prior"                                                ///
         admico0  = "Yr of adopt"                                               ///
         admico1  = "1 yr after"                                                ///
         admico2  = "2 yr after"                                                ///
         admico3  = "3 yr after"                                                ///
         mico4    = "4+ yr after")                                              ///
         vertical                                                                          ///
         yline(0)                                                                          ///
         ytitle("Log points")                                                              ///
         xtitle("Time passage relative to year of adoption of implied contract exception") ///
         addplot(line @b @at)                                                              ///
         ciopts(recast(rcap))                                                              ///
         rescale(100)                                                                      ///
         scheme(s1mono)

graph export "figurename.png", replace

```


> 后记：本质上是根据多期 DID 的回归设计，获得系数和对应标准误信息，然后利用标记置信区间的 `connected` 进行绘图。


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表1](https://www.jianshu.com/p/de82fdc2c18a) 
- [Stata连享会推文列表2](https://github.com/arlionn/Stata_Blogs/blob/master/README.md)
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)



---
![欢迎加入Stata连享会(公众号: StataChina)](http://upload-images.jianshu.io/upload_images/7692714-aac735aa45404445.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")

