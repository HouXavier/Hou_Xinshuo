> 作者：侯新烁 ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)) 

> **编者按：** 本期给大家介绍一个根据变量信息快速创建分组的命令 `astile` ，推文内容根据 **fintechprofessor** 网站提供的介绍短文整理而成，并对命令作用和安装等进行了补充说明，通过示例帮助大家如何快速实现数据分组。

> 原文地址：http://fintechprofessor.com/create-portfolios-in-stata-using-astile/。

> 程序作者：Attaullah Shah  （attaullah.shah@imsciences.edu.pk）

---

## 1. astile : 一个简明介绍

`astile` 可根据已有变量创建一个按 1 到 n 的等级排列的分级变量。例如，我们可能有兴趣创建 10 个以公司大小为基础区分的公司分级组合。也就是将包含最小 10% 数值的公司设定为第 1 组，接下来的 10% 设定为第 2 组，依次类推， `astile` 新建了一个新变量数值在 1，2，3... 直到 n 之间变化，其中 n 是分组数的最大值，在 `nq()` 选项中予以指定。例如，如果我们想创建 10 个分组，新的变量的数值会在 1 到 10 之间。

`astile` 比 state 官方提供的 `xtile` 命令处理速度更快。 它的高效性在数据集较大或者当分组类别被多次创建时更加明显，比如说，我们可能需要根据每个年份或者月份分别创建分组。与 state 官方命令 `xtile` 不同，`astile` 是 byable 的，意即可以通过 `bys` 命令分组进行多维变量的分组生成。`astile` 处理分组运算时超级有效率。比如说，在在使用 `bys` 和不使用 `bys` 的情况下去处理有一百万观测值和 1000 个组别的数据时，通常只有几秒钟的时间差异。

## 2. astile : 版本与更新
该命令的作者对 `astile` 的功能和使用上进行了调整形成多个版本，本次推文是基于其最新版本 （ **3.2.2   : 17Apr2018** ）展开，因此使用前需查看其版本信息，并进行更新。

可使用 `viewsource` 或者 `adoedit` 查看该命令的创建和版本信息

```stata
viewsource  astile.ado
```

```
*! 3.0.0  :  1Apr2017 , Added speed efficiency
*! Author : Attaullah Shah: attaullah.shah@imsciences.edu.pk
* In this current version, I have added more speed efficiency
* Also, this version fixes bugs related to [IF] and [IN] options when used with by(varlist)
* Verion 	2.0.0 	12Mar2017
*! Version 	1.0.0	 27Jun2015
```
以上信息说明已安装命令并非最新版本，使用如下命令进行更新

```stata
ssc install astile,replace
```
```
*! 3.2.2   : 17Apr2018, fixed a bug that would occur when IF and BYS were used together
*! 3.2.1   : 09Nov2017, added option qc (quantile criteria) for making qunatile boundaries on a given criteria
*! Author  : Attaullah Shah: attaullah.shah@imsciences.edu.pk
*! 3.0.0   : 1Apr2017 , Added speed efficiency
*! Version : 2.0.0 	12Mar2017
*! Version : 1.0.0	 27Jun2015
```

## 3. 使用案例：

#### 案例1：根据公司的市场价值创建10个分组
   在这个例子中，我们将使用 `grunfeld.dta` 数据集，并从 stata 的服务器下载该数据。

```stata
. webuse grunfeld 
. astile size10 = mvalue,  nq(10)
```


  在以上命令中，我们生成了一个名为 `size10` 的新变量，其取值从 1 到 10 。数值的排序是基于现有变量 `mvalue` 生成。 在逗号之后，我们设定选项 `nq(10)` ,让 `astile` 生成 10 个分组。如果我们想要生成 20 个组，那么选项将是 `nq(20)` 。

#### 案例2：各年重复分级

  若想要根据公司的市场价值按年份分别生成 5 个组别，可以使用 `bysort` 或者 `bys` 选项完成这个任务。这次我们把新变量命名为 `size5` 。

```stata
. webuse grunfeld 
. bys year : astile size5   = mvalue, nq(5)
. * 或者使用如下命令，效果相同
.            astile size5_2 = mvalue, nq(5) by(year)
```


#### 案例3：在数据子集上创建分位数断点

  如果分位数断点需要以数据的子集为基础的话，我们可以用选项 `qc()` ，然后整个数据集中的观察值被分配给这些断点。这样的计算在检验资产定价模型中是很常见的。股票的分组在金融分析中通常是一个标准程序。研究者们经常使用诸如公司规模、账面市值比、投资额等变量进行股票的分组。也有少数研究者用工资的子样本去计算数据的十分位数断点，然后将公司分配给不同的分位组别。比如说：一个人可能会根据约证券交易所上市股票的市值排名确定各不同十分位数组段的断点值，然后将将纽约证券交易所、美国证券交易所和纳斯达克的所有公开交易股票分别分配到其中的一个分组中。使用 `astile` 实现上述操作将非常容易。

让我们用一个例子说明它是如何实现的。首先，我们生成一个虚拟数据集，创建 500 个公司从 1951 到 2050 总计 100 年的数据，以及这500家公司对应的三家证券交易所：纽约证券交易所、美国证券交易所和纳斯达克。最后，生成一个各年各家公司有差异的市场资本化变量（ `MarkCap` ）。


```stata
. clear
. set obs 500
. gen company=_n
. expand 100
. bys company: gen year=_n+1950
. bys company: gen xc=mod(company, 3)+1
. gen exchange = cond(xc==1, "NYSE", cond(xc==2, "AMEX", "NASDAQ"))
. gen MarkCap=uniform()*10000
```

此时，我们将根据纽约证券交易所上市的公司的 `MarkCap` 变量设定断点值，然后并将所有股票对应的公司分配到不同的分位数组别中去。

```
. bys year: astile size = MarkCap, qc(exchange =="NYSE") nq(10)
```

此时，我们可以根据变量分组进行一些结果展示。


```
. tab size
. tab size if exchange =="NYSE"
```

#### 4. 示例代码汇总


```stata
. clear
. set obs 500
. gen company=_n
. expand 100
. bys company: gen year=_n+1950
. bys company: gen xc=mod(company, 3)+1
. gen exchange = cond(xc==1, "NYSE", cond(xc==2, "AMEX", "NASDAQ"))
. gen MarkCap=uniform()*10000
. replace MarkCap = MarkCap+2000 if exchange =="NYSE"

.           astile size01   = MarkCap,  nq(10)
. bys year: astile size02   = MarkCap,  nq(10)
.           astile size02_2 = MarkCap,  nq(10) by(year)
. bys year: astile size03   = MarkCap,  nq(10) qc(exchange =="NYSE") 
.           astile size03_2 = MarkCap,  nq(10) qc(exchange =="NYSE")  by(year)

. tab2 size01 size02 size03
. tab  size03
. tab  size03 if exchange =="NYSE"
```


> 后记：有了 `astile` ，想分几组分几组！！


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表1](https://www.jianshu.com/p/de82fdc2c18a) 
- [Stata连享会推文列表2](https://github.com/arlionn/Stata_Blogs/blob/master/README.md)
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)



---
![欢迎加入Stata连享会(公众号: StataChina)](http://upload-images.jianshu.io/upload_images/7692714-aac735aa45404445.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")

