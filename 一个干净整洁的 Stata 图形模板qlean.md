
> 作者：侯新烁 ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)) 

> Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

> **编者按：** 计量画图时，Stata 在默认情况下的图形输出通常为浅蓝色带横向网格底纹的图形，且不同输出图形通常以差异化的颜色加以标注，但很多时候，期刊排版要求图片为可识别的（如黑白风格或者不同标记风格）图片。因此，本次推文为大家介绍一个用户编写的干净而整洁的Stata图形模版 **`qlean`**，并结合我们常用的示例数据为大家展示了一下对比效果。

> 下载链接：https://github.com/rangerqu/qlean

---

## 1. 导言

Stata默认的背景学名称为 light bluish gray，但通常情况下若不加以修饰，图形可能常常不能达到 **使用者的审美要求** ，此时我们可以通过设定绘图模版 `scheme` 的方式来快捷更改图形的整体风格。例如，Stata官方提供了彩色模板 `s2color` 以及两个黑白模版 `s1mono` 和 `s2mono`,在我们往期推文中也曾向读者们介绍过  `tufte` 、 `burd` 、 `lean1` 和 `lean2` 等绘图模版，详见 [**Stata：中文期刊风格的纯黑白图形**](https://www.jianshu.com/p/425ab13feb8e) https://www.jianshu.com/p/425ab13feb8e 。此外，现在已经可以使用外部命令 `grstyle` 随意更改 Stata 图形模板了，可通过修改设定以使其适用于期刊的黑白模式。

但对于懒人，或者一定程度上使用  `grstyle` 命令更改 Stata 模板的操作起来还并不熟练的筒子们，`qlean` 模版还是很亲民和易用的。

## 2. 下载方法


该图形模版为作者保存在其 GitHub 账户的个人项目中，链接地址为 https://github.com/rangerqu/qlean ，读者可通过 **Clone or Download** 按钮，选择 **Download ZIP** 将程序包下载。

解压后，将 `scheme-qlean.scheme` `scheme-qleanmono.scheme` 两文件复制到 Stata 程序的 附加 ado 文件夹中的文件夹 `s` ，如笔者电脑为 `D:\stata15\ado\plus\s` ; 将 `.style` 为后缀的 color 设定文件 复制到文件夹 `style` , 如 `D:\stata15\ado\plus\style`。此时即可使用该模版。


![scheme文件](https://images.gitee.com/uploads/images/2018/1103/162526_ece7111f_2109590.png "scheme.png")

![style文件](https://images.gitee.com/uploads/images/2018/1103/162554_42fb0405_2109590.png "style.png")


**关于 `ssc install` 或 `net install` 的说明：** 笔者通过 Stata 的 `findit` 和 `ssc` 等命令进行了安装实验，并未搜索到与之匹配的已发布命令包，因此，我们暂时也只能通过调用本地文件位置的方式进行安装。命令方法如下 `net install qlean， from（"~本地qlean文件夹存储位置"）` ，其中引用位置部分需根据自己下载的文件包位置确定。同时还需要补充 `.pkg` 和 `stata.toc` 文件，因此本次推文仅做说明，如有疑问可联系推文作者（ `houxinshuo@126.com` ）。

## 3. 应用方法

为展示其模版效果，将以 Stata 默认风格为对比基础进行展示。

#### 3.1 散点图

```stata
. sysuse "auto.dta", clear  
. twoway scatter price weight 
. twoway scatter price weight, scheme(qlean)
. twoway scatter price weight, scheme(qleanmono)
```
**结果展示:**

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/212934_ef140360_2109590.png "s1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/214344_87933f75_2109590.png "q1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/214356_9de492e9_2109590.png "q11.png")

```stata
. twoway scatter price weight if foreign==0 ||  scatter price weight if foreign==1
. twoway scatter price weight if foreign==0 ||  scatter price weight if foreign==1, scheme(qlean)
. twoway scatter price weight if foreign==0 ||  scatter price weight if foreign==1, scheme(qleanmono)
```

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/214054_09deb1cf_2109590.png "s2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/214105_15931566_2109590.png "q2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/214115_ee471a9c_2109590.png "q21.png")

#### 3.2 折线图

```stata
. sysuse uslifeexp,clear

. twoway line le_male   year || line le_female year
. twoway line le_male   year || line le_female year, scheme(qlean)
. twoway line le_male   year || line le_female year, scheme(qleanmono)
```

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/215032_93b23d2b_2109590.png "l1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/215047_177b4a74_2109590.png "l2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/215056_366b1e2b_2109590.png "l21.png")

#### 3.3 矩阵图

```stata
sysuse lifeexp, clear
gen lgnppc = ln(gnppc)
gr matrix popgr lexp lgnp safe
gr matrix popgr lexp lgnp safe, scheme(qlean)
gr matrix popgr lexp lgnp safe, scheme(qleanmono)
```

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/221005_8d64021f_2109590.png "m.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/221015_74acd027_2109590.png "m2.png")

此处，使用 mono 风格时结果看起来并无差别，所以 `qlean` 风格结果仅展示一副。


### 3.4 条形图

```stata
. sysuse nlsw88, clear
. graph hbar (mean) wage, over(smsa) over(married) over(collgrad)	
. graph hbar (mean) wage, over(smsa) over(married) over(collgrad) scheme(qlean)	
```


![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/222017_940cf012_2109590.png "h1.png")


![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/222025_d919ea86_2109590.png "h2.png")


## 4. 几个推荐的黑白模版

结合往期推文和本期介绍的 `qlean` ，为大家提供几个效果图，方便使用时灵活选择自己中意的图形风格。主要涉及的风格模版有 `tufte` 、 `burd` 、 `lean1` 和 `lean2`，如安装方法不可用，可尝试使用 `findit` 命令搜寻。另外，我们可以通过 `mcolor()` 选项选择 `qlean` 模版的色彩搭配风格。

安装方法：

```stata
. ssc install scheme_tufte, replace 
. ssc install scheme-burd, replace  
. net install gr0002_3.pkg  
```

应用示例：
```stata
. sysuse auto, clear
. twoway lfitci mpg weight || scatter mpg weight
```

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/224532_bd410157_2109590.png "f1.png")

```stata
. twoway lfitci mpg weight || scatter mpg weight, scheme(qlean) mc(ply3)
```

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/224545_0ee50174_2109590.png "f2.png")

```stata
. twoway lfitci mpg weight || scatter mpg weight, scheme(tufte)
```
![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/224557_976c9877_2109590.png "ft.png")

```stata
. twoway lfitci mpg weight || scatter mpg weight, scheme(burd)
```

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/224608_7f2db12b_2109590.png "fb.png")

```stata
. twoway lfitci mpg weight || scatter mpg weight, scheme(lean1)
```

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/224625_b6e59763_2109590.png "fl1.png")

```stata
. twoway lfitci mpg weight || scatter mpg weight, scheme(lean2)
```

![输入图片说明](https://images.gitee.com/uploads/images/2018/1103/224634_d3303c46_2109590.png "fl2.png")


## 5. 代码汇总


```stata
. sysuse "auto.dta", clear  
. twoway scatter price weight 
. twoway scatter price weight, scheme(qlean)
. twoway scatter price weight, scheme(qleanmono)

. twoway scatter price weight if foreign==0 ||  scatter price weight if foreign==1
. twoway scatter price weight if foreign==0 ||  scatter price weight if foreign==1, scheme(qlean)
. twoway scatter price weight if foreign==0 ||  scatter price weight if foreign==1, scheme(qleanmono)

. sysuse uslifeexp,clear

. twoway line le_male   year || line le_female year
. twoway line le_male   year || line le_female year, scheme(qlean)
. twoway line le_male   year || line le_female year, scheme(qleanmono)

. sysuse lifeexp, clear
. gen lgnppc = ln(gnppc)
. gr matrix popgr lexp lgnp safe
. gr matrix popgr lexp lgnp safe, scheme(qlean)
. gr matrix popgr lexp lgnp safe, scheme(qleanmono)

. sysuse nlsw88, clear
. graph hbar (mean) wage, over(smsa) over(married) over(collgrad)	
. graph hbar (mean) wage, over(smsa) over(married) over(collgrad) scheme(qlean)	

. ssc install scheme_tufte, replace 
. ssc install scheme-burd, replace  
. net install gr0002_3.pkg  

. sysuse auto, clear
. twoway lfitci mpg weight || scatter mpg weight
. twoway lfitci mpg weight || scatter mpg weight, scheme(qlean) mc(ply3)
. twoway lfitci mpg weight || scatter mpg weight, scheme(tufte)
. twoway lfitci mpg weight || scatter mpg weight, scheme(burd)
. twoway lfitci mpg weight || scatter mpg weight, scheme(lean1)
. twoway lfitci mpg weight || scatter mpg weight, scheme(lean2)
```


> 后记：输出整洁的图形就是这么简单！


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表1](https://www.jianshu.com/p/de82fdc2c18a) 
- [Stata连享会推文列表2](https://github.com/arlionn/Stata_Blogs/blob/master/README.md)
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)



---
![欢迎加入Stata连享会(公众号: StataChina)](http://upload-images.jianshu.io/upload_images/7692714-aac735aa45404445.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")

